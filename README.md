**General utility POSIX shell scripts.**

* `file-checksums` - Print checksums of a file using all available checksum commands.

* `gen-inc-guard` - Generate an unique 'include guard' for a C/C++ header.

* `clean-cmake-build-dir` - Remove all files created by running cmake in the build directory.

* `list-bad-symlinks` - Print a verbose list of all broken symlinks residing in the current directory.

* `list-firejailable` - Print a list of binaries for which a dedicated firejail profile file exists.

**Gentoo-specific utility scripts.**

* `make-ebuild-install` - Generate a minimal ebuild on the fly so that the source code residing in the current directory can be compiled, and the resulting binary installed (and later uninstalled) using portage. Source must be configured (using ./configure, cmake or similar) prior to running this script.

* `make-kernel` - Automate the compilation and instalation of kernels and allow inheriting custom KCFLAGS/KCXXFLAGS/KLDFLAGS from make.conf.

* `package-versions-online` - Fetch a list of available versions of a package from Gentoo website.

* `world-update` - Sync the portage tree and perform all the steps needed for a full system upgrade.

* `list-foss-games` - Print a list of all not-yet-installed free software games avaliable in the portage tree.

* `qwhich` - Print the name of the package that provides the specified command.
